import {AnyDocument} from "dynamoose/dist/Document";

export enum BoxStatus {
  PRINTED = 'printed',
  COOKED = 'cooked',
  IN_DELIVERY = 'in delivery',
  DELIVERED = 'delivered',
  CANCELLED = 'cancelled',
}

export enum WeekDay {
  MONDAY = "monday",
  TUESDAY = "tuesday",
  WEDNESDAY = "wednesday",
  THURSDAY = "thursday",
  FRIDAY = "friday",
  SATURDAY = "saturday",
  SUNDAY = "sunday",
  WITHOUT_DAY = "withoutDay",
}

export interface Entity extends AnyDocument {
  id: string,
  updatedAt: number,
  createdAt: number,
  deleted: "-" | "+",
}

export interface Role extends Entity {
  name: string,
}

export interface Dish {
  name: string;
  dishType: string;
  quantity: number;
  weekDay: WeekDay;
}

export interface User<TRole extends string | Role> extends Entity {
  email: string,
  role: TRole[],
  firstName?: string,
  lastName?: string,
  avatar?: string,
}

export type SimpleUser = User<string>;
export type ExtendedUser = User<Role>;

export interface Customer extends Entity {
  wpId?: number;
  company?: string;
  firstName?: string;
  lastName?: string;
  email: string;
  phoneNumber?: string;
}

export interface Box<TRole extends string | Role,
  TUser extends string | User<TRole>,
  TCustomer extends string | Customer,
  TCoordinates extends string | Coordinates<TRole, TUser>,
  TAddress extends string | Address<TRole, TUser, TCoordinates>,
  TOrder extends string | Order<TRole, TUser, TCustomer, TCoordinates, TAddress>> extends Entity {
  order: TOrder;
  sticker: string;
  boxStatus: BoxStatus;
  qrCode: string;
  customerComment?: string;
  weekDay: WeekDay;
}

export type SimpleBox = Box<string, string, string, string, string, string>;
export type ExtendedBox = Box<Role,
  User<Role>,
  Customer,
  Coordinates<Role, User<Role>>,
  Address<Role, User<Role>, Coordinates<Role, User<Role>>>,
  Order<Role, User<Role>, Customer, Coordinates<Role, User<Role>>, Address<Role, User<Role>, Coordinates<Role, User<Role>>>>>

export interface Address<TRole extends string | Role,
  TUser extends string | User<TRole>,
  TCoordinates extends string | Coordinates<TRole, TUser>> extends Entity {
  address1: string;
  address2: string;
  city: string;
  postCode: string;
  coordinates?: TCoordinates,
}

export type SimpleAddress = Address<string, string, string>;
export type ExtendedAddress = Address<Role, User<Role>, Coordinates<Role, User<Role>>>;

export interface Coordinates<TRole extends string | Role, TUser extends string | User<TRole>> extends Entity {
  latitude: number;
  longitude: number;
  assignedDriver?: TUser;
  verified?: boolean;
  name?: string;
}

export type SimpleCoordinates = Coordinates<string, string>;
export type ExtendedCoordinates = Coordinates<Role, User<Role>>;

export interface Order<TRole extends string | Role,
  TUser extends string | User<TRole>,
  TCustomer extends string | Customer,
  TCoordinates extends string | Coordinates<TRole, TUser>,
  TAddress extends string | Address<TRole, TUser, TCoordinates>> extends Entity {
  orderNumber?: string;
  orderStatus: "processing" | "completed" | "cancelled";
  customer?: TCustomer;
  dishes: Dish[],
  address: TAddress;
  finalPrice: number;
  customerComment?: string;
}

export type SimpleOrder = Order<string, string, string, string, string>;
export type ExtendedOrder = Order<Role, User<Role>, Customer, Coordinates<Role, User<Role>>, Address<Role, User<Role>, Coordinates<Role, User<Role>>>>

export type NewUser = Pick<SimpleUser, "id" | "email" | "role" | "firstName" | "lastName" | "avatar">;
export type NewOrder = Pick<SimpleOrder, "orderNumber" | "orderStatus" | "customer" | "dishes" | "customerComment" | "address" | "finalPrice">;
export type NewCustomer = Pick<Customer, "wpId" | "company" | "firstName" | "lastName" | "email" | "phoneNumber">;
export type NewBox = Pick<SimpleBox, "id" | "order" | "sticker" | "boxStatus" | "qrCode" | "customerComment" | "weekDay">;
export type NewAddress = Pick<SimpleAddress, "address1" | "address2" | "city" | "postCode" | "coordinates">;
export type NewCoordinates = Pick<SimpleCoordinates, "latitude" | "longitude" | "assignedDriver" | "verified" | "name">;
export type NewRole = Pick<Role, "name">;


